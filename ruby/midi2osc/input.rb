#!/usr/bin/env ruby

dir = File.dirname(File.expand_path(__FILE__))
$LOAD_PATH.unshift dir + "/../lib"

require "unimidi"
require "osc-ruby"
require "osc-ruby/em_server"

@client = OSC::Client.new('127.0.0.1', 1337)

# Prompts the user to select a midi input
# Sends an inspection of the first 10 messages messages that input receives to standard out

num_messages = 10

# Prompt the user
input = UniMIDI::Input.gets

# using their selection...

puts "send some MIDI to your input now..."

while 1 do
  m = input.gets
  data = m[0][:data]
  timestamp = m[0][:timestamp]
  
  data.each do |d|
    puts(d)
    if d == 145 
  
      puts("#{timestamp} - #{data}")
      @client.send( OSC::Message.new("/ds0/trigger", "hullo!"))
    
    end
   
  end

end

puts "finished"
