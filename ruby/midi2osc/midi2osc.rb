#!/usr/bin/env ruby
dir = File.dirname(File.expand_path(__FILE__))
$LOAD_PATH.unshift dir + "/../lib"

require "rubygems"
require "unimidi"
require "osc-ruby"
require "osc-ruby/em_server"
require "micromidi"


@client = OSC::Client.new( '192.168.0.101', 1337 )

# Prompts the user to select a midi input
# Sends an inspection of the first 10 messages messages that input receives to standard out

num_messages = 10

# Prompt the user
#input = UniMIDI::Input.gets
@input = UniMIDI::Input.use(:first)
@output = UniMIDI::Output.use(:first)


# using their selection...

puts "send some MIDI to your input now..."



MIDI.using(@input, @output) do

  thru_except :note

  receive :note do |message|
    message.octave += 1 if %w{C E G}.include?(message.note_name)
    output message
  end

  join

end



#@client.send( OSC::Message.new( "/ds0/trigger" , "hullo!" ))

puts "finished"
