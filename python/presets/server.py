#!/usr/bin/env python3
# Jóhann Berentsson 2016
from pythonosc import dispatcher
from pythonosc import osc_server

from preset import Preset

# TODO: Make it send the number of patch files in folder as OSC message.
# TODO: Make a file that starts all programs needed in projects

if __name__ == "__main__":
    #try:
    preset = Preset()
    preset.SetArguments()

    # Addresses to listen to
    dispatcher = dispatcher.Dispatcher()
    dispatcher.map("/preset", preset.SetPreset, "pattern")

    args = preset.GetArgs()

    server = osc_server.ThreadingOSCUDPServer((args.serverip, args.serverport), dispatcher)
    print("Serving on " + args.serverip + ":" + str(args.serverport))
    server.serve_forever()

    #except:
    #    print(":( Running the server failed.")