#!/usr/bin/env python3
# Jóhann Berentsson 2016
import unittest
from preset import Preset

# TODO: Configure test json data
# TODO: Finish the cases for the functions that are already in class

class TestStringMethods(unittest.TestCase):
#    def test_upper(self):
#        self.assertEqual('foo'.upper(), 'FOO')
#
#    def test_isupper(self):
#        self.assertTrue('FOO'.isupper())
#        self.assertFalse('Foo'.isupper())
#
#    def test_split(self):
#        s = 'hello world'
#        self.assertEqual(s.split(), ['hello', 'world'])
#        # check that s.split fails when the separator is not a string
#        with self.assertRaises(TypeError):
#            s.split(2)

    def set_mock(self):
        self.data = {
           "value": "as",
           "id": "0",
           "type": "sequencer",
           "p": "asdf",
           "development": True
        }

    def test_Router(self):
        preset = Preset()

        self.set_mock()
        self.assertTrue(preset.Router(self.data))
        self.assertTrue(preset.Router(self.data))

    def test_ReadFile(self):
        preset = Preset()
        preset.Main()

        self.assertTrue(preset.ReadFile())

    def test_Sequencer(self):
        preset = Preset()
        preset.Main()

        self.set_mock()
        self.assertTrue(preset.Sequencer(self.data))

    def test_Arguments(self):
        preset = Preset()
        preset.Main()
        data = preset.GetArgs()

        # Are the arguments set
        # Client
        self.assertEqual(data.clientport, 9997)
        self.assertEqual(data.clientip, "127.0.0.1")

        # Server
        self.assertEqual(data.serverport, 9998)
        self.assertEqual(data.serverip, "127.0.0.1")

        # Misc
        self.assertEqual(data.id, 0)
        self.assertEqual(data.file, "presets.json")

if __name__ == '__main__':
    unittest.main()