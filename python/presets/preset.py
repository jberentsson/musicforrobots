#!/usr/bin/env python3
# Jóhann Berentsson 2016

import argparse
import random
import time
import json
import os
import math
import subprocess

from pprint import pprint
from pythonosc import dispatcher
from pythonosc import osc_server
from pythonosc import osc_message_builder
from pythonosc import udp_client

# TODO make it read data for all modules.
# TODO a function that checks the type of data int, string, array etc and sends out the data accordingly
# TODO add function to read OSC from PD and write it to a new json file.
# TODO add function for a name of a patch, description that will be printed in the console?
# TODO change the format of the preset file to OSC data "/sequencer/0/settings/enabled 0" one per each line inside json with name an all that stuff

# Get the file
class Preset:
    def Main(self):
        try:
            self.SetArguments()
            data = self.ReadFile()
            self.CreateMessages(data)
        except:
            # Return error
            print(":( Initiating class failed.")

    def SetArguments(self):
        parser = argparse.ArgumentParser()
        parser.add_argument("--serverip", default="127.0.0.1", help="The ip of the OSC server")
        parser.add_argument("--clientip", default="127.0.0.1", help="The ip to listen on")
        parser.add_argument("--clientport", type=int, default=9997, help="The port the OSC server is listening on")
        parser.add_argument("--serverport", type=int, default=9998, help="The port the OSC server is listening on")
        parser.add_argument("--file", default="presets.json", help="The port the OSC server is listening on")
        parser.add_argument("--id", default=0, help="The id of the preset.")

        self.args = parser.parse_args()
        
    # Read the json file
    def ReadFile(self):
        try:
            # Path for the patch files configured
            path = "../../json/" + str(self.args.file)
            with open(path) as file:
                data = json.load(file)
                print("Opening " + path)
                return data
        except:
            # Return error
            print(":( Unable to read file. \"" + str(self.args.file)) + "\""
            return 0

    # Case for the sequencer module
    def Sequencer(self, v):
        data = {
            "address": "/" + v['type'] + "/" + v['id'] + "/" + v['p'], 
            "value": v['value'] 
        }
        return data

    # Case for the router module
    def Router(self, v):
        data = {
            "address": "/" + v['type'] + "/" + v['id'] + "/" + v['p'], 
            "value": v['value'] 
        }
        return data

    def CheckType(self, data):
        if data["type"] == "sequencer":
            return self.Sequencer(data)
        elif  data["type"] == "router":
            return self.Router(data)
        #else:
            # Nothing yet
        #key = data['type']

    # Construct the OSC message
    def CreateMessages(self, modules):
        
        for module in modules:
            for p in module['settings'].keys():

                data = {
                    "value": str(module['settings'][p]), 
                    "id": str(module["id"]), 
                    "type": str(module["type"]), 
                    "p": p
                }
                print(data)

                    #options = { 
                    #    "sequencer": ,
                    #    "router": 
                    #}

                
                d = self.CheckType(data)
                self.SendMessage(d)
                return 1
                #try:
                #except:
                    # Return error
               #     print(":( Creating messages failed.")
               #     return 0
                
    # Send the OSC data
    def SendMessage(self, data):
        try:
	        msg = osc_message_builder.OscMessageBuilder(address = data['address'])
	        msg.add_arg(data['value'])
	        msg = msg.build()

	        client = udp_client.UDPClient(self.args.serverip, self.args.serverport)
	        client.send(msg)
	            
	        print(str(data['address']) + " " + str(data['value']))
	        return 1

        except:
            # Return error
            print(":( Sending meessage failed.")
            return 0

    def GetArgs(self):
        return self.args

    def SetPreset(self, one, args, pattern):
        id = str(pattern)
        print("Pattern: " + id)
        subprocess.call(["./send.py","--ip" , self.args.clientip, "--patch", id])

if __name__ == "__main__":
    # Run the stuff
    preset = Preset()
    preset.Main()
