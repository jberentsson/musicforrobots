#!/usr/bin/env python3
import argparse
import random
import time
import json

from pprint import pprint

from pythonosc import osc_message_builder
from pythonosc import udp_client

def ReadFile(args):
    path = "../../json/sett/" + str(args.patch) + ".json"

    with open(path) as file:
        data = json.load(file)
        return data

def BuildMessage(args):
    for parameter in args["data"]:
    	# TODO athuga hvernig þetta á að fara fram. Mér sýnist þessi if hluti ekki vera að virka.
        if id in args:
            address = "/" + args["type"] + "/" + str(args["id"]) + "/" + parameter
        else:
            address = "/" + args["type"] + "/" + parameter

        msg = osc_message_builder.OscMessageBuilder(address = address)

        if type(args["data"][parameter]) == list:
            for value in args["data"][parameter]:
                msg.add_arg(value)
        else:
            msg.add_arg(args["data"][parameter])

        msg = msg.build()
        client.send(msg)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--ip", default="127.0.0.1", help="The ip of the OSC server")
    parser.add_argument("--port", type=int, default=9997, help="The port the OSC server is listening on")
    parser.add_argument("--patch", type=int, default=0, help="The port the OSC server is listening on")
    args = parser.parse_args()

    client = udp_client.UDPClient(args.ip, args.port)

    data = ReadFile(args);
    for module in data:
        BuildMessage(module)