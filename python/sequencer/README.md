## DrumMachine ##
Drummachine written in Python that sends out triggers via Open Sound Control(OSC).

The presets are stored in the presets.json file.

## Arguments ##
--ip -i 
--port -n
--bpm -t
--pattern -p
--bank -b
--address -a

## TODO ##
Add dynamic track number so sequences can have n tracks.