#!/usr/bin/env python3
import unittest
from oscdm import *

class TestStringMethods(unittest.TestCase):
    def test_metro(self):
        s = Sequencer()
        o = s.Metro()
        equal = [
            {
                "bpm" : 120,
                "division": 16
            },
            {
                "bpm" : 90,
                "division": 8
            },
            {
                "bpm" : 160,
                "division": 32
            },
            {
                "bpm" : 120,
                "division": 16
            },
            {
                "bpm" : 120,
                "division": 8
            }
        ]

        # Test if the conversion from bpm to bpm is right
        for case in equal:
            s.args.bpm = case['bpm']
            s.args.division = case['division']
            self.assertEqual(case['division']/case['bpm'], s.Metro())

    def test_step(self):
        s = Sequencer()
        s.sequence = {
            "0":[1,0,0,0]
        }

        #o = s.Step(0)

if __name__ == '__main__':
    unittest.main()