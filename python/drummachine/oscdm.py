#!/usr/bin/env python3
# Jóhann Berentsson 2016

import argparse
import random
import time
import json
import os

from pprint import pprint
from pythonosc import osc_message_builder
from pythonosc import udp_client

class Sequencer():
    def __init__(self):
        self.args = self.ReadArgs()
        if __name__ == "__main__":
            self.sequence = self.ReadFile(self.args.pattern)
            try:
                self.Loop()
            except:
                print("Error")

    def Trigger(self, n):
        a = self.args
        client = udp_client.UDPClient(a.ip, a.port)
        # address = a.address + n
        msg = osc_message_builder.OscMessageBuilder(address = "/ds" + n + "/trigger")
        msg = msg.build()
        return client.send(msg)

    def Step(self, n):
        s = self.sequence
        for track in s:
            length = len(s[track])
            if s[track][n%length] == 1:
                self.Trigger(track)

    # Reads the bpm and returns the time between steps in ms.
    def Metro(self):
        a = self.args
        return a.division/a.bpm

    def PrintSequence(self):
        s = self.sequence
        print(s)
        for track in s:
            pprint(s[track])

    def ReadFile(self, n):
        a = self.args
        try:
            f = open(a.file, 'r')
            data = json.loads(f.read())
            return data[a.pattern]
        except:
            print("Reading the pattern file failed!")

    def Loop(self):        
        i = 0
        self.PrintSequence()
        while 1:    
            self.Step(i)    
            i += 1
            time.sleep(self.Metro())

    # Program flag arguments
    def ReadArgs(self):
        parser = argparse.ArgumentParser(description="Process some integers.")
        parser.add_argument("-i", "--ip", default="127.0.0.1", help="The ip of the OSC server")
        parser.add_argument("-n", "--port", type=int, default=1337, help="The port the OSC server is listening on")
        parser.add_argument("-t", "--bpm",  type=int, default=130, help="The tempo of the sequencer.")
        parser.add_argument("-p", "--pattern", type=int, default=0, help="The pattern to be played.")
        parser.add_argument("-o", "--file", type=str, default="patterns.json", help="Name of the pattern file.")
        parser.add_argument("-a", "--address", type=str, default="/trigger/ds" , help="Name of the pattern file.")
        parser.add_argument("-d", "--division", type=int, default=16 , help="Time division for the sequencer.")

        return parser.parse_args()

if __name__ == "__main__":
    Sequencer()