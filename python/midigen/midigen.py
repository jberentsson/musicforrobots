#!/usr/bin/env python3
# Jóhann Berentsson 2017
# The program generates midifiles with a drum pattern.
import json
import logging

logging.basicConfig(filename='debug.log',level=logging.DEBUG)

class IO:
    """ Input output """
    @staticmethod
    def read_json(file):
        """ Read the json file """
        logging.info("Reading JSON file " + file + ".json")
        try:
            with open(file) as json_data:
                return json.load(json_data)
        except:
            logging.debug("Error reading \""+ file +"\".")

    @staticmethod
    def read_file(file):
        """ Read files """
        logging.info("Reading file.")
        try:
            file = open(file, "r")
            return file.read()
        except:
            logging.debug("Error reading \""+ file +"\".")

    @staticmethod
    def write_file(data, name):
        logging.info("Writing to midi file " + name + ".mid")
        try:
            """ Writes data to file """
            file = open(name + ".mid", "w")
            file.write(data)
            file.close()
        except:
            logging.debug("Writing to file failed.")

def to_binary(dec, size):
    """ Converts decimal number to binary """
    if size:
        bits = ""
        for i in range(size):
            bits += str((dec >> i) & 0x1)
        return str(bits[::-1]) # To MSB
    else:
        return 0

def make_note(note, velocity, data):
    """ Make midi note """
    note_data = {
        data["noteon"] + to_binary(note, 8) + to_binary(velocity, 8),
        data["noteoff"] + to_binary(note, 8) + to_binary(velocity, 8)
    }
    return note_data

def make_file(data):
    """ make the midi file"""
    try:
        string = ""
        for row in data:
            row = hex(int(row, 2)) # Convert binary to hex string.
            string += str(row) + "\n" # Add the line to the file.
        return string
    except:
        logging.debug("Couldn't write to midi file.")

def bin_to_hex(bin):
    """ converts binary string to hex """
    return len(bin) / 4

def test():
    #dataz = toBinary(13, 4)
    #dataz += toBinary(1, 4)
    #dataz += toBinary(5, 4)
    stuff = "{} {} {}".format("einn", "tveir", "þrír")
    print(stuff)

def generate_pattern(patterns):
    """ Generate the complete pattern """
    logging.info("Generating pattern.")
    try:
        shit = len(patterns)
        return shit
    except:
        logging.debug("Generating pattern failed.")

def main():
    """ The main function """
    logging.info("Program started.")
    data = IO.read_json("default.json")
    note = make_note(66, 127, data)
    midi_data = make_file(note)
    IO.write_file(midi_data, "default")

    generate_pattern(data["patterns"])

    logging.info("Program Finished")

if __name__ == '__main__':
    main()
    test()
