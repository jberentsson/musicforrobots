## Description ##
The program reads drum patterns from a JSON file and generates a polyrithmic drum pattern and saves it as a midi file.

### Links ###
http://www.music.mcgill.ca/~ich/classes/mumt306/StandardMIDIfileformat.html