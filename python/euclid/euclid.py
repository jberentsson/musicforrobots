#!/usr/bin/env python3
# Jóhann Berentsson 2016

import argparse
import random
import time
import json
import os

from pprint import pprint
from pythonosc import osc_message_builder
from pythonosc import udp_client

parser = argparse.ArgumentParser(description='Process some integers.')
parser.add_argument("-i", "--ip", default="127.0.0.1", help="The ip of the OSC server")
parser.add_argument("-n", "--port", type=int, default=1337, help="The port the OSC server is listening on")
parser.add_argument("-t", "--bpm",  type=int, default=130, help="The tempo of the sequencer.")
parser.add_argument("-p", "--pattern", type=int, default=0, help="The pattern to be played.")
parser.add_argument("-b", "--bank", type=str, default="patterns.json", help="Name of the pattern file.")
args = parser.parse_args()

client = udp_client.UDPClient(args.ip, args.port)

def Trigger(n):
	msg = osc_message_builder.OscMessageBuilder(address = "/trigger/ds" + str(n))
	msg = msg.build()
	return client.send(msg)

def Step(n):
	for track in sequence:
		length = len(sequence[track])
		if sequence[track][n%length] == 1:
			Trigger(track)

def Metro(bpm):
	return 16/bpm

def PrintSequence(sequence):
	for track in sequence:
		pprint(sequence[track])

def ReadFile(n):
	try:
		f = open(args.bank, 'r')
		patterns = json.loads(f.read())
		return patterns[args.pattern]
	except:
		print("Reading the pattern file failed!")

i = 0
metro = Metro(args.bpm)
sequence = ReadFile(args.pattern)



try:
	PrintSequence(sequence)
	while 1:
		Step(i)
		i += 1
		time.sleep(metro)
except:
	print("Error")
