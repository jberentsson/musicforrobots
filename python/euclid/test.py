#!/usr/bin/env python3

def GCD(a, b):
    if b == 0:
        return a
    else:
        return GCD(b, a%b)

i = 0
while i < 16:
    out = GCD(3, i)
    print(out)
    i += 1