#!/usr/bin/env python3
import argparse
import math
import os
import signal
import subprocess
import time

from pprint import pprint
from pythonosc import osc_message_builder
from pythonosc import udp_client

def Main():
    a = args
    client = udp_client.UDPClient(a.ip, a.port)
    msg = osc_message_builder.OscMessageBuilder(a.address)
    
    if a.value:
        msg.add_arg(a.value)
    
    msg = msg.build()
    print(a.address + " "+ str(a.value))
    return client.send(msg)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--ip",
        default="127.0.0.1", help="The ip to listen on")
    parser.add_argument("--port",
        type=int, default=1336, help="The port to listen on")
    parser.add_argument("--value",
        type=int, default=False, help="Argument for the OSC command")
    parser.add_argument("--address",
        type=str, default="/pad", help="Osc command")
    args = parser.parse_args()

    #pprint(args)

    Main()
