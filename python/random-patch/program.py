#!/usr/bin/env python3


# The os.setsid() is passed in the argument preexec_fn so
# it's run after the fork() and before  exec() to run the shell.
"""Small example OSC server
This program listens to several addresses, and prints some information about
received packets.
"""
import argparse
import math
import os
import signal
import subprocess
import time

from pythonosc import dispatcher
from pythonosc import osc_server
from pythonosc import osc_message_builder
from pythonosc import udp_client

# Start the midi2osc pure data file
def Midi2OSCInit():
    pid = subprocess.Popen(["pd", "-nogui", "-nodac", "-alsamidi", "../../pd/midi2osc.pd"]).pid
    time.sleep(1)
    print("Midi2OSC pid:" + str(pid))
    return pid

# Start the drumsynth pure data file
def SynthInit():
    pid = subprocess.Popen(["pd", "-nogui", "-alsa", "../../project/sett.pd"]).pid
    time.sleep(1)
    print("Synth pid:" + str(pid))
    return pid

def print_volume_handler(unused_addr, args, volume):
    print("[{0}] ~ {1}".format(args[0], volume))

def print_compute_handler(unused_addr, args, volume):
    try:
        print("[{0}] ~ {1}".format(args[0], args[1](volume)))
    except ValueError: pass

def HandlePad(a, b):
    sequencer_pid = 0
    if b == 0:
        print("Start")
        sequencer_pid = StartSeq(b)
    elif b == 1:
        print("Stop - " + str(puredata))
        if sequencer_pid != 0:
            os.killpg(sequencer_pid, signal.SIGTERM)
    elif b == 2:
        print("Pause")
    elif b == 3:
        print("Record")
    elif 11 < b < 16:
        Trigger(b%4)
        print("Trigger " + str(b))
    #else:
        # Nothing yet!

    print(str(a) + " " + str(b))


def Trigger(n):
    client = udp_client.UDPClient(args.ip, 1337)
    msg = osc_message_builder.OscMessageBuilder("/ds"+str(n)+"/trigger")
    #msg.add_arg(value)
    msg = msg.build()
    return client.send(msg)

def StartSeq(pattern):
    variables = [
        "../drummachine/oscdm.py",
         "--ip",
         args.ip,
         "--file",
         "../drummachine/patterns.json",
          "-p",
         str(pattern%4)
    ]
    sequencer = subprocess.Popen(variables).pid
    return sequencer

def KillProcess():
    try:
        subprocess.Popen(["kill", str(sequencer_pid)]) 
        print("KillKill!")
    except:
        print("No processes running!")


def ReadArguments():
    parser = argparse.ArgumentParser()
    parser.add_argument("--ip",
        default="127.0.0.1", help="The ip to listen on")
    parser.add_argument("--port",
        type=int, default=1336, help="The port to listen on")
    return parser.parse_args()

def Main(args):
    dis = dispatcher.Dispatcher()
    dis.map("/pad", HandlePad)
    dis.map("/filter", print)
    dis.map("/volume", print_volume_handler, "Volume")
    dis.map("/logvolume", print_compute_handler, "Log volume", math.log)

    server = osc_server.ThreadingOSCUDPServer(
        (args.ip, args.port), dis)
    print("Serving on {}".format(server.server_address))
    server.serve_forever()


if __name__ == "__main__":
    args = ReadArguments()
    puredata = Midi2OSCInit()
    #synth = SynthInit()

    Main(args)