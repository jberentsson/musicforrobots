#!/usr/bin/env python3
# Jóhann Berentsson 2016

import argparse
import random
import time
import json
import os

from pprint import pprint
from pythonosc import osc_message_builder
from pythonosc import udp_client
from itertools import repeat


class Sequencer():
    def __init__(self):
        self.args = self.ReadArgs()
        #self.sequence = self.ReadFile(self.args.pattern)
        try:
            self.Run()
        except:
            print("Error")

    def Run(self):

        addresses = [
            "/ds0",
            "/ds1",
            "/ds2",
            "/ds3"
        ]

        for address in addresses:
            address += "/frequency"
            value = random.randint(0, 127)
            self.Send(address, value)

        for address in addresses:
            address += "/volume"
            value = 0.9
            self.Send(address, value)

        for address in addresses:
            address += "/envelope"
            self.Send(address + "/attack", 0)
            self.Send(address + "/decay", 1)
            self.Send(address + "/sustain", 0.95)
            self.Send(address + "/release", 150)
            #address = ""

        addresses = [
            "/mixer0/volume0",
            "/mixer1/volume1",
            "/mixer2/volume2",
            "/mixer3/volume3"
        ]

        address = ""
        i = 0
        n = 4
        while i <= n:
            j = 0
            while j <= 6:
                address += "/mixer" + str(i)
                address += "/volume" + str(j)
                value = 0.9
                self.Send(address, value)
                j += 1
                address = ""
                
            i += 1
            
    def Send(self, address, value):
        a = self.args
        client = udp_client.UDPClient(a.ip, a.port)
        msg = osc_message_builder.OscMessageBuilder(address)
        msg.add_arg(value)
        msg = msg.build()

        print(address + str(value))

        return client.send(msg)

    # Program flag arguments
    def ReadArgs(self):
        parser = argparse.ArgumentParser(description="Process some integers.")
        parser.add_argument("-i", "--ip", default="127.0.0.1", help="The ip of the OSC server")
        parser.add_argument("-n", "--port", type=int, default=1337, help="The port the OSC server is listening on")
        parser.add_argument("-t", "--bpm",  type=int, default=130, help="The tempo of the sequencer.")
        parser.add_argument("-p", "--pattern", type=int, default=0, help="The pattern to be played.")
        parser.add_argument("-o", "--file", type=str, default="patterns.json", help="Name of the pattern file.")
        parser.add_argument("-a", "--address", type=str, default="/trigger/ds" , help="Name of the pattern file.")
        parser.add_argument("-d", "--division", type=int, default=16 , help="Time division for the sequencer.")

        return parser.parse_args()

if __name__ == "__main__":
    Sequencer()
