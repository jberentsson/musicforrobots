#!/usr/bin/env python3

import random

i = 0
list = []


def RandomInt():
    integer = random.randint(0, 15)
    if integer in list:
        RandomInt()
        print("In list!")
    else:
        list.append(integer)
        print("Not in list!")


while i < 16:
    RandomInt()
    i += 1

out = ""

for number in list:
    out += " " + str(number)

print(out)
