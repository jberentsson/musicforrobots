#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Jóhann Berentsson 2016

import sys

from PyQt5 import QtCore, QtGui, QtWidgets

from gui import mainwindow, aboutwindow, midiwindow, oscwindow, presetswindow # Windows
from view import ryk, progio, progsettings, sequencer, presets, clock, settings # Program classes.

class MainWindow(QtWidgets.QMainWindow, mainwindow.Ui_MainWindow, aboutwindow.Ui_AboutWindow, ryk.Ryk, progio.Progio, clock.Clock, progsettings.ProgSettings, sequencer.Sequencer):
    def __init__(self):
        super(MainWindow, self).__init__()
        self.setupUi(self)
        self.SettingsInit()
        self.SeqInit()
        self.IOInit() # Set UI parameters.
        self.RykInit() # Ryk
        self.ClockInit()
        self.ServerStart()

        # Subwindows
        self.OSCWindow = None
        self.MidiWindow = None
        self.PresetsWindow = None
        self.AboutWindow = None

        # Menu triggers
        # File
        self.actionQuit.triggered.connect(self.closeEvent)

        # Settings
        self.actionOSC.triggered.connect(self.WindowOSC)
        self.actionMIDI.triggered.connect(self.WindowMIDI)
        self.actionPresets.triggered.connect(self.WindowPresets)

        # help
        self.actionAbout.triggered.connect(self.WindowAbout)

    def WindowOSC(self):
        if self.OSCWindow is None:
            self.OSCWindow = OSCWindow(self)
        self.OSCWindow.show()
        
    def WindowMIDI(self):
        if self.MidiWindow is None:
            self.MidiWindow = MidiWindow(self)
        self.MidiWindow.show()
        
    def WindowPresets(self):
        if self.PresetsWindow is None:
            self.PresetsWindow = PresetsWindow(self)
        self.PresetsWindow.show()

    def WindowAbout(self):
        if self.AboutWindow is None:
            self.AboutWindow = AboutWindow(self)
        self.AboutWindow.show()

    def CloseSubWindows(self):
        # Close SubWindow
        if self.OSCWindow:
            self.OSCWindow.close()

        if self.MidiWindow:
            self.MidiWindow.close()

        if self.PresetsWindow:
            self.PresetsWindow.close()

        if self.AboutWindow:
            self.AboutWindow.close()

        # Close MainWindow
        self.close()

    # The main close event 
    def closeEvent(self, event):
        self.CloseSubWindows()


class OSCWindow(QtWidgets.QMainWindow, oscwindow.Ui_OSCWindow):
    def __init__(self, parent=None):
        super(OSCWindow, self).__init__()
        self.setupUi(self)

        # Buttons
        #self.accept.connect(self.UpdateOSCSettings)

        #self.lineServerAddress.setText(MainWindow.ServerAddress)
        #self.lineServerPort.setText(str(MainWindow.ServerPort))
        #self.lineClientAddress.setText(MainWindow.ClientAddress)
        #self.lineClientPort.setText(str(MainWindow.ClientPort))

    def Close(self):
        self.close()

class MidiWindow(QtWidgets.QMainWindow, midiwindow.Ui_MidiWindow):
    def __init__(self, parent=None):
        super(MidiWindow, self).__init__()
        self.setupUi(self)

    def Close(self):
        self.close()

class PresetsWindow(QtWidgets.QMainWindow, presetswindow.Ui_PresetsWindow, presets.Presets):
    def __init__(self, parent=None):
        super(PresetsWindow, self).__init__()
        self.setupUi(self)
        self.PresetInit()

    def Close(self):
        self.close()

class AboutWindow(QtWidgets.QMainWindow, aboutwindow.Ui_AboutWindow):
    def __init__(self, parent=None):
        super(AboutWindow, self).__init__()
        self.setupUi(self)

        self.pushCloseWindow.clicked.connect(self.Close)

    def Close(self):
        self.close()

if __name__ == "__main__":
    App = QtWidgets.QApplication(sys.argv)
    ui = MainWindow()
    ui.show()

    sys.exit(App.exec_())