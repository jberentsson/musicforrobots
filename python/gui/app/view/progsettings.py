#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Jóhann Berentsson 2016

from pythonosc import osc_message_builder
from pythonosc import udp_client

from pythonosc import dispatcher
from pythonosc import osc_server

import threading

class ProgSettings(object):
    def SettingsInit(self):
        # Parameters
        # Server
        self.ServerAddress = "127.0.0.1"
        self.ServerPort = 8001
        
        # Client
        self.ClientAddress = "127.0.0.1"
        self.ClientPort = 8000

    def SendData(self, a, value):
        #print("data!!!!")
        client = udp_client.UDPClient(self.ClientAddress, int(self.ClientPort))

        msg = osc_message_builder.OscMessageBuilder(address = a)
        msg.add_arg(value)
        msg = msg.build()
        client.send(msg)

    def UpdateOSCSettings(self):
        self.ClientAddress = self.lineClientAddress.text()
        self.ClientPort = self.lineClientPort.text()

    def OSCserver(self):
        self.dispatcher = dispatcher.Dispatcher()
        self.dispatcher.map("/test", self.print)
        server = osc_server.ThreadingOSCUDPServer((self.ServerAddress, self.ServerPort), self.dispatcher)
        print("Serving on {}".format(server.server_address))
        server.serve_forever()
        
        #Thread(target=self.OSCserver).exit()
        
    def print(self, a, b):
        print("A: " + str(a))
        print("B: " + str(b))
        self.lineSeqChannel.setText(str(b))
        Thread.Thread(target=self.OSCserver).start()

        #p = Process(target=self.OSCserver)
        #p.start()
        #p.join()()