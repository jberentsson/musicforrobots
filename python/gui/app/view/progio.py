#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Jóhann Berentsson 2016

class Progio(object):
    def IOInit(self):
        self.controllers = [
            {
                "name": "Alesis Fusion",
                "channel": 1
            },
            {
                "name": "Harpsichord",
                "channel": 2
            }
        ]
        
        self.keyboards = [
            {
                "name": "Alesis Fusion",
                "channel": 1
            },
            {
                "name": "Harpsichord",
                "channel": 2
            }
        ]
        
        #for c in self.controllers:
        #    self.listControllers.addItem(c["name"]) 

        #for k in self.keyboards:
        #    self.listKeyboards.addItem(k["name"]) 

        # Test 
        #self.btnServerStart.clicked.connect(self.ServerStart)

    def ServerStart(self):
        a = "/test"
        d = "list 1 2 3 4 5 6"
        self.SendData(a, d)

        a = "/seq0 /division"
        d = "3"
        self.SendData(a, d)
        
        a = "/seq0 /swing"
        d = "33"
        self.SendData(a, d)
        
        a = "/seq0 /transpose"
        d = "3"
        self.SendData(a, d)