#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Jóhann Berentsson 2016

from PyQt5 import QtCore, QtGui, QtWidgets

class Clock(object):
    def ClockInit(self):
        print("Clock Init")

        # Buttons
        self.btnClockStart.clicked.connect(self.StartStop)
        self.btnClockReset.clicked.connect(self.ClockReset)

        # Parameters
        self.SeqActive = 1


    def StartStop(self):
        if self.SeqActive == 0:
            self.SeqActive = 1
            print("Start")
            self.SendData("/metro", "start")
        else:
            self.SeqActive = 0
            print("Stop")
            self.SendData("/metro", "stop")

    def ClockReset(self):
        print("Clock Reset!")
        self.SendData("/metro", "reset")
