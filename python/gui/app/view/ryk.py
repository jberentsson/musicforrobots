#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Jóhann Berentsson 2016

class Ryk(object):
    def RykInit(self):
        self.UpdateNotes()

        # valueChanged()	Emitted when the slider's value has changed. The tracking() determines whether this signal is emitted during user interaction.
        # sliderPressed()	Emitted when the user starts to drag the slider.
        # sliderMoved()	Emitted when the user drags the slider.
        # sliderReleased()	Emitted when the user releases the slider.

        self.SliderStatus = 0 # Is one of the sliders pressed.

        # Sliders pressed
        self.verticalNote_0.sliderPressed.connect(self.SliderPressed)
        self.verticalNote_1.sliderPressed.connect(self.SliderPressed)
        self.verticalNote_2.sliderPressed.connect(self.SliderPressed)
        self.verticalNote_3.sliderPressed.connect(self.SliderPressed)
        self.verticalNote_4.sliderPressed.connect(self.SliderPressed)
        self.verticalNote_5.sliderPressed.connect(self.SliderPressed)
        self.verticalNote_6.sliderPressed.connect(self.SliderPressed)
        self.verticalNote_7.sliderPressed.connect(self.SliderPressed)
        self.verticalNote_8.sliderPressed.connect(self.SliderPressed)
        self.verticalNote_9.sliderPressed.connect(self.SliderPressed)
        self.verticalNote_10.sliderPressed.connect(self.SliderPressed)
        self.verticalNote_11.sliderPressed.connect(self.SliderPressed)
        self.verticalNote_12.sliderPressed.connect(self.SliderPressed)
        self.verticalNote_13.sliderPressed.connect(self.SliderPressed)
        self.verticalNote_14.sliderPressed.connect(self.SliderPressed)
        self.verticalNote_15.sliderPressed.connect(self.SliderPressed)

        # Sliders Value Changed
        self.verticalNote_0.valueChanged.connect(self.UpdateNotes)
        self.verticalNote_1.valueChanged.connect(self.UpdateNotes)
        self.verticalNote_2.valueChanged.connect(self.UpdateNotes)
        self.verticalNote_3.valueChanged.connect(self.UpdateNotes)
        self.verticalNote_4.valueChanged.connect(self.UpdateNotes)
        self.verticalNote_5.valueChanged.connect(self.UpdateNotes)
        self.verticalNote_6.valueChanged.connect(self.UpdateNotes)
        self.verticalNote_7.valueChanged.connect(self.UpdateNotes)
        self.verticalNote_8.valueChanged.connect(self.UpdateNotes)
        self.verticalNote_9.valueChanged.connect(self.UpdateNotes)
        self.verticalNote_10.valueChanged.connect(self.UpdateNotes)
        self.verticalNote_11.valueChanged.connect(self.UpdateNotes)
        self.verticalNote_12.valueChanged.connect(self.UpdateNotes)
        self.verticalNote_13.valueChanged.connect(self.UpdateNotes)
        self.verticalNote_14.valueChanged.connect(self.UpdateNotes)
        self.verticalNote_15.valueChanged.connect(self.UpdateNotes)

        # Sliders Value Changed
        self.verticalNote_0.sliderReleased.connect(self.SliderRelease)
        self.verticalNote_1.sliderReleased.connect(self.SliderRelease)
        self.verticalNote_2.sliderReleased.connect(self.SliderRelease)
        self.verticalNote_3.sliderReleased.connect(self.SliderRelease)
        self.verticalNote_4.sliderReleased.connect(self.SliderRelease)
        self.verticalNote_5.sliderReleased.connect(self.SliderRelease)
        self.verticalNote_6.sliderReleased.connect(self.SliderRelease)
        self.verticalNote_7.sliderReleased.connect(self.SliderRelease)
        self.verticalNote_8.sliderReleased.connect(self.SliderRelease)
        self.verticalNote_9.sliderReleased.connect(self.SliderRelease)
        self.verticalNote_10.sliderReleased.connect(self.SliderRelease)
        self.verticalNote_11.sliderReleased.connect(self.SliderRelease)
        self.verticalNote_12.sliderReleased.connect(self.SliderRelease)
        self.verticalNote_13.sliderReleased.connect(self.SliderRelease)
        self.verticalNote_14.sliderReleased.connect(self.SliderRelease)
        self.verticalNote_15.sliderReleased.connect(self.SliderRelease)

        # Length spin boxes.
        self.spinLength_0.valueChanged.connect(self.UpdateLength)
        self.spinLength_1.valueChanged.connect(self.UpdateLength)
        self.spinLength_2.valueChanged.connect(self.UpdateLength)
        self.spinLength_3.valueChanged.connect(self.UpdateLength)
        self.spinLength_4.valueChanged.connect(self.UpdateLength)
        self.spinLength_5.valueChanged.connect(self.UpdateLength)
        self.spinLength_6.valueChanged.connect(self.UpdateLength)
        self.spinLength_7.valueChanged.connect(self.UpdateLength)
        self.spinLength_8.valueChanged.connect(self.UpdateLength)
        self.spinLength_9.valueChanged.connect(self.UpdateLength)
        self.spinLength_10.valueChanged.connect(self.UpdateLength)
        self.spinLength_11.valueChanged.connect(self.UpdateLength)
        self.spinLength_12.valueChanged.connect(self.UpdateLength)
        self.spinLength_13.valueChanged.connect(self.UpdateLength)
        self.spinLength_14.valueChanged.connect(self.UpdateLength)
        self.spinLength_15.valueChanged.connect(self.UpdateLength)

        # Step spin boxes.
        self.spinPattern_0.valueChanged.connect(self.UpdatePattern)
        self.spinPattern_1.valueChanged.connect(self.UpdatePattern)
        self.spinPattern_2.valueChanged.connect(self.UpdatePattern)
        self.spinPattern_3.valueChanged.connect(self.UpdatePattern)
        self.spinPattern_4.valueChanged.connect(self.UpdatePattern)
        self.spinPattern_5.valueChanged.connect(self.UpdatePattern)
        self.spinPattern_6.valueChanged.connect(self.UpdatePattern)
        self.spinPattern_7.valueChanged.connect(self.UpdatePattern)
        self.spinPattern_8.valueChanged.connect(self.UpdatePattern)
        self.spinPattern_9.valueChanged.connect(self.UpdatePattern)
        self.spinPattern_10.valueChanged.connect(self.UpdatePattern)
        self.spinPattern_11.valueChanged.connect(self.UpdatePattern)
        self.spinPattern_12.valueChanged.connect(self.UpdatePattern)
        self.spinPattern_13.valueChanged.connect(self.UpdatePattern)
        self.spinPattern_14.valueChanged.connect(self.UpdatePattern)
        self.spinPattern_15.valueChanged.connect(self.UpdatePattern)

    def SliderPressed(self):
        self.SliderStatus = 1

    # Send the note data when slider is released
    def SliderRelease(self):
        self.SliderStatus = 0
        # Read the values from the sliders.
        data = [
            self.verticalNote_0.value(),
            self.verticalNote_1.value(),
            self.verticalNote_2.value(),
            self.verticalNote_3.value(),
            self.verticalNote_4.value(),
            self.verticalNote_5.value(),
            self.verticalNote_6.value(),
            self.verticalNote_7.value(),
            self.verticalNote_8.value(),
            self.verticalNote_9.value(),
            self.verticalNote_10.value(),
            self.verticalNote_11.value(),
            self.verticalNote_12.value(),
            self.verticalNote_13.value(),
            self.verticalNote_14.value(),
            self.verticalNote_15.value()
        ]

        i = 0
        for note in data:
            self.SendData("/note" + str(i) , note)
            i += 1

    def UpdateNotes(self):
        # Update note values in textboxes in each step.
        self.lineNote_0.setText(str(self.verticalNote_0.value()))
        self.lineNote_1.setText(str(self.verticalNote_1.value()))
        self.lineNote_2.setText(str(self.verticalNote_2.value()))
        self.lineNote_3.setText(str(self.verticalNote_3.value()))
        self.lineNote_4.setText(str(self.verticalNote_4.value()))
        self.lineNote_5.setText(str(self.verticalNote_5.value()))
        self.lineNote_6.setText(str(self.verticalNote_6.value()))
        self.lineNote_7.setText(str(self.verticalNote_7.value()))
        self.lineNote_8.setText(str(self.verticalNote_8.value()))
        self.lineNote_9.setText(str(self.verticalNote_9.value()))
        self.lineNote_10.setText(str(self.verticalNote_10.value()))
        self.lineNote_11.setText(str(self.verticalNote_11.value()))
        self.lineNote_12.setText(str(self.verticalNote_12.value()))
        self.lineNote_13.setText(str(self.verticalNote_13.value()))
        self.lineNote_14.setText(str(self.verticalNote_14.value()))
        self.lineNote_15.setText(str(self.verticalNote_15.value()))

    def SetSequencer(self, seq):
        notes = [
            self.verticalNote_0,
            self.verticalNote_1,
            self.verticalNote_2,
            self.verticalNote_3,
            self.verticalNote_4,
            self.verticalNote_5,
            self.verticalNote_6,
            self.verticalNote_7,
            self.verticalNote_8,
            self.verticalNote_9,
            self.verticalNote_10,
            self.verticalNote_11,
            self.verticalNote_12,
            self.verticalNote_13,
            self.verticalNote_14,
            self.verticalNote_15
        ]

        length = [
            self.spinLength_0,
            self.spinLength_1,
            self.spinLength_2,
            self.spinLength_3,
            self.spinLength_4,
            self.spinLength_5,
            self.spinLength_6,
            self.spinLength_7,
            self.spinLength_8,
            self.spinLength_9,
            self.spinLength_10,
            self.spinLength_11,
            self.spinLength_12,
            self.spinLength_13,
            self.spinLength_14,
            self.spinLength_15
        ]

        pattern = [
            self.spinPattern_0,
            self.spinPattern_1,
            self.spinPattern_2,
            self.spinPattern_3,
            self.spinPattern_4,
            self.spinPattern_5,
            self.spinPattern_6,
            self.spinPattern_7,
            self.spinPattern_8,
            self.spinPattern_9,
            self.spinPattern_10,
            self.spinPattern_11,
            self.spinPattern_12,
            self.spinPattern_13,
            self.spinPattern_14,
            self.spinPattern_15
        ]

        i = 0
        while i < 16:
            notes[i].setValue(seq["notes"][i])
            length[i].setValue(seq["length"][i])
            pattern[i].setValue(seq["pattern"][i])
            i += 1

    def UpdateLength(self):
        print("Length Updated!")

    def UpdatePattern(self):
        print("Pattern Updated!")