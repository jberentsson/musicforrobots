#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Jóhann Berentsson 2016

class Settings(object):
    def SettingsInit(self):
        # Parameters
        self.ServerAddress = "127.0.0.1"
        self.ServerPort = 8001
        self.ClientAddress = "127.0.0.1"
        self.ClientPort = 8000