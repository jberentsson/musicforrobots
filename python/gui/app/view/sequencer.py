#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Jóhann Berentsson 2016

class Sequencer(object):
    def SeqInit(self):
        data = {
            "step": 2,
            "division": 2, 
            "transpose":2,
            "swing": 2,
            "id": 2,
            "length": 2,
            "note": 2
        }

        self.FormSequencer(data)

    def FormSequencer(self, d):
        self.spinSeqStep.setValue(d["step"])
        self.spinSeqDivision.setValue(d["division"])
        self.spinSeqTranspose.setValue(d["transpose"])
        self.spinSeqSwing.setValue(d["swing"])
        self.spinSeqID.setValue(d["id"])
        self.lineSeqLength.setText(str(d["length"]))
        self.lineSeqNote.setText(str(d["note"]))