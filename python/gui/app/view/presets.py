#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Jóhann Berentsson 2016


class Presets(object):
    def PresetInit(self):
        print("Presets!")


        self.data = [
            {
                "id": 1,
                "name": "Einn"
            },
            {
                "id": 2,
                "name": "Tveir"
            },
            {
                "id": 3,
                "name": "Þrír"
            },
            {
                "id": 4,
                "name": "Fjórir"
            }
        ]

        for p in self.data:
            self.listPresets.addItem(p["name"])