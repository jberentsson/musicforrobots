#!/usr/bin/env python3
import argparse
import math

from tkinter import *

from pythonosc import dispatcher
from pythonosc import osc_server

#def print_volume_handler(unused_addr, args, volume):
#  print("[{0}] ~ {1}".format(args[0], volume))

#def print_compute_handler(unused_addr, args, volume):
#  try:
#    print("[{0}] ~ {1}".format(args[0], args[1](volume)))
# except ValueError: pass

#if __name__ == "__main__":
#    parser = argparse.ArgumentParser()
#    parser.add_argument("--ip",
#      default="127.0.0.1", help="The ip to listen on")
#    parser.add_argument("--port",
#      type=int, default=5005, help="The port to listen on")
#    args = parser.parse_args()

#    dispatcher = dispatcher.Dispatcher()
#    dispatcher.map("/debug", print)
#    dispatcher.map("/volume", print_volume_handler, "Volume")
#    dispatcher.map("/logvolume", print_compute_handler, "Log volume", math.log)

#    server = osc_server.ThreadingOSCUDPServer(
#      (args.ip, args.port), dispatcher)
#    print("Serving on {}".format(server.server_address))
#    server.serve_forever()

class Application(Frame):
    def say_hi(self):
        print("hi there, everyone!")

    def createWidgets(self):
        self.QUIT = Button(self)
        self.QUIT["text"] = "QUIT"
        self.QUIT["fg"]   = "red"
        self.QUIT["command"] =  self.quit

        self.QUIT.pack({"side": "left"})

        self.hi_there = Button(self)
        self.hi_there["text"] = "Hello",
        self.hi_there["command"] = self.say_hi

        self.hi_there.pack({"side": "left"})

    def __init__(self, master=None):
        Frame.__init__(self, master)
        self.pack()
        self.createWidgets()

root = Tk()
app = Application(master=root)
app.mainloop()
root.destroy()