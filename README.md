## musicforrobots ##
Collection of audio and midi apps written in Python and Pure Data. Filenames and other stuff will get changed as this projects contiues development.

## Requirements ##
- Python 3
- Pure Data Vanilla
- PyQt5

## Pure Data Libraries ##
- list-abs
- freeverb~
- cyclone
- mrpeach
- maxlib
- mjlib 

## Features ##
- MIDI clock input.
- MIDI Router.
- Arpeggiator.
- Chord sustainer.
- Chord memory.
- Drummachine conventional x0x and other methods.
- SH101 style sequencer.
- 16 Step sequencer inspired by Ryk 100m sequencer.
- MIDI interfacing between PD and hardware synthesisezrs such as Arturia MiniBrute and AKAI MPC 1000.
- Export and import patches as json files.
- Modules use Open Sound Control to send messages between each other and for communications between other apps or devices.

## Samples ##
The default sample directory is "samples/"

## License ##
MIT 
